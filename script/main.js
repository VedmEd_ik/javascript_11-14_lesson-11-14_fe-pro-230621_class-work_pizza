'use strict'
document.addEventListener('DOMContentLoaded', () => {
	// Ціни на інгредієнти
	const defaultPrice = {
		smallSize: 100,
		middleSize: 125,
		bigSize: 150,
		ketchupSauce: 15,
		bbqSauce: 15,
		ricottaSauce: 18,
		ordinaryCheeseSauce: 8,
		fetaCheeseSauce: 9,
		mozzarellaCheeseSauce: 10,
		vealSauce: 12,
		tomatoesSauce: 11,
		mushroomsSauce: 10,
	};

	// Піца клієнта
	const clientPizza = {
		size: null,
		sauce: [],
		nameSauce: [],
		topping: [],
		nameTopping: [],
		totalPrice: null,
	};

	// Функція вибору розміру піци
	const pizzaSize = document.getElementById('pizza');
	pizzaSize.addEventListener('click', checkSize);
	function checkSize({ target }) {
		clientPizza.size = target.value;
		showTotalInfoForPizza()
	};

	// Функція підрахунку загальної суми
	function totalPrice() {
		// Обнулення загальної ціни піци
		if (clientPizza.totalPrice != 0) { clientPizza.totalPrice = 0; }

		// Додавання до суми ціни коржа
		switch (clientPizza.size) {
			case 'small':
				clientPizza.totalPrice += defaultPrice.smallSize;
				break;
			case 'mid':
				clientPizza.totalPrice += defaultPrice.middleSize;
				break;
			case 'big':
				clientPizza.totalPrice += defaultPrice.bigSize;
				break;
		};

		// Додавання до суми ціни соусу
		clientPizza.sauce.forEach(element => {
			switch (element) {
				case "sauceClassic":
					clientPizza.totalPrice += defaultPrice.ketchupSauce;
					break;
				case "sauceBBQ":
					clientPizza.totalPrice += defaultPrice.bbqSauce;
					break;
				case "sauceRikotta":
					clientPizza.totalPrice += defaultPrice.ricottaSauce;
					break;
			}
		});

		// Додавання до суми ціни топінгу
		clientPizza.topping.forEach(element => {
			switch (element) {
				case "moc1":
					clientPizza.totalPrice += defaultPrice.ordinaryCheeseSauce;
					break;
				case "moc2":
					clientPizza.totalPrice += defaultPrice.fetaCheeseSauce;
					break;
				case "moc3":
					clientPizza.totalPrice += defaultPrice.mozzarellaCheeseSauce;
					break;
				case "telya":
					clientPizza.totalPrice += defaultPrice.vealSauce;
					break;
				case "vetch1":
					clientPizza.totalPrice += defaultPrice.tomatoesSauce;
					break;
				case "vetch2":
					clientPizza.totalPrice += defaultPrice.mushroomsSauce;
					break;
			}
		});
	};

	// Функція вивдення на екран загальної ціни
	function showTotalPrice() {
		document.querySelector('.price > p').textContent = `Цiна: ${clientPizza.totalPrice} грн.`;
	}
	// Функція для виведення соусів
	function showTotalSauces() {
		document.querySelector('.sauces > p').textContent = `Соуси: ${clientPizza.nameSauce.join(', ')}`;
	};

	// Функція для виведення топінгів
	function showTotalTopings() {
		document.querySelector('.topings > p').textContent = `Топiнги: ${clientPizza.nameTopping.join(', ')}`;
	};

	// Функція для виведення на екран загальної інформації по замовленню піци
	function showTotalInfoForPizza() {
		totalPrice();
		showTotalPrice();
		showTotalSauces();
		showTotalTopings();
	}

	// Функція drag and drop
	const dragAndDrop = () => {
		const defaultPizza = document.querySelector('.table-wrapper .table');

		function dragStart(event) {
			// Сортування інгредієнтів по атрибуту data- та додавання їх назви в масив
			const ingridient = event.target.dataset.key;
			if (ingridient === 'sauce') {
				if (!clientPizza.nameSauce.includes(event.target.alt)) {
					clientPizza.nameSauce.push(event.target.alt);
					clientPizza.sauce.push(event.target.id);
				}
			} else if (ingridient === 'topping') {
				if (!clientPizza.nameTopping.includes(event.target.alt)) {
					clientPizza.nameTopping.push(event.target.alt);
					clientPizza.topping.push(event.target.id);
				}
			};

			event.dataTransfer.effectAllowed = 'move';
			event.dataTransfer.setData("img", this.attributes.src.textContent);
		};

		function dragEnd(event) {
			defaultPizza.classList.remove('hovered');
			if (event.preventDefault) event.preventDefault();
			if (event.stopPropagation) event.stopPropagation();
		};
		function dragOver(event) {
			if (event.preventDefault) event.preventDefault();
			if (event.stopPropagation) event.stopPropagation();
		};
		function dragEnter(event) {
			defaultPizza.classList.add('hovered');
			if (event.preventDefault) event.preventDefault();
			if (event.stopPropagation) event.stopPropagation();
		};
		function dragLeave(event) {
			defaultPizza.classList.remove('hovered');
			if (event.preventDefault) event.preventDefault();
			if (event.stopPropagation) event.stopPropagation();
		};

		function dragDrop(event) {
			if (event.preventDefault) event.preventDefault();
			if (event.stopPropagation) event.stopPropagation();


			const imgIngridient = document.createElement('img');
			imgIngridient.setAttribute('src', event.dataTransfer.getData('img'));
			defaultPizza.append(imgIngridient);

			showTotalInfoForPizza();
		};

		// Присвоюємо обробник для цільового обєкта (зони перетягування)
		defaultPizza.addEventListener('dragover', dragOver);
		defaultPizza.addEventListener('drop', dragDrop);
		defaultPizza.addEventListener('dragenter', dragEnter);
		defaultPizza.addEventListener('dragleave', dragLeave);

		// Присвоюємо обробник для обєкта, який перетягуємо
		const ingridients = document.querySelectorAll('.ingridients .draggable');
		ingridients.forEach(ingridient => {
			ingridient.addEventListener('dragstart', dragStart);
			ingridient.addEventListener('dragend', dragEnd);
		});

	}

	// Запуск dragAndDrop
	dragAndDrop();

	/* ======================================================================= */

	// Невловима знижка
	const banner = document.getElementById('banner');
	banner.addEventListener('mouseover', (event) => {
		banner.style.right = Math.random() * (document.documentElement.clientWidth - event.target.offsetWidth) + 'px';
		banner.style.bottom = Math.random() * (document.documentElement.clientHeight - event.target.offsetHeight) + 'px';
	});

	/* ======================================================================= */

	// Кнопка "Скинути"
	document.getElementById('resetButton').addEventListener('mousedown', event => {
		event.target.classList.add('button-clicked');
	});
	document.getElementById('resetButton').addEventListener('mouseup', event => {
		event.target.classList.remove('button-clicked');
	});
	document.getElementById('resetButton').addEventListener('click', () => {

		// Видаляємо вибраний розмір піци
		document.querySelectorAll('#pizza .radioIn').forEach(size => {
			size.checked = '';
		})

		// Видаляємо інгредієнти на коржі
		const ingridients = document.querySelectorAll('.table img');
		ingridients.forEach(ingridient => {
			if (!ingridient.alt[0]) {
				ingridient.remove();
			}
		})

		// Обнуляємо піцу
		clientPizza.size = null;
		clientPizza.sauce = [];
		clientPizza.nameSauce = [];
		clientPizza.topping = [];
		clientPizza.nameTopping = [];
		clientPizza.totalPrice = null;

		showTotalInfoForPizza();
	}
	)

	/* ======================================================================= */
	const form = document.forms[1];
	const btnSubmit = form[4];
	btnSubmit.addEventListener('mousedown', event => {
		btnSubmit.classList.add('button-clicked');
	});
	btnSubmit.addEventListener('mouseup', event => {
		btnSubmit.classList.remove('button-clicked');
	});

	// Валідація і відправка форми

	form.addEventListener('submit', formValidate);
	let formValidation = false;

	for (let i = 0; i < form.length; i++) {
		form[i].onchange = inputValidate;
	}

	// Валідація input
	function inputValidate(event) {
		const patternForName = /[A-Za-zА-Яа-яЁёА-ЯаяІіЇї\']/,
			patternForTel = /\+[0-9]{12}/,
			patternForEmail = /([A-z0-9_.-]{1,})@([A-z0-9_.-]{1,}).([A-z]{2,8})/,
			inputName = this.name,
			inputValue = this.value;

		switch (inputName) {
			case "name":
				if (inputValue.search(patternForName) === -1) {
					this.classList.add('error');
					formValidation = false;
				} else {
					this.classList.remove('error');
					formValidation = true;
				}
				break;
			case "phone":
				if (inputValue.search(patternForTel) === -1) {
					this.classList.add('error');
					formValidation = false;
				} else {
					this.classList.remove('error');
					formValidation = true;
				}
				break;
			case "email":
				if (inputValue.search(patternForEmail) === -1) {
					this.classList.add('error');
					formValidation = false;
				} else {
					this.classList.remove('error');
					formValidation = true;
				}
				break;
		};
	};

	// Відправка форми
	function formValidate(event) {
		event.preventDefault();
		if (formValidation === true) {
			form.submit();
			window.location.href = './thank-you.html';
		}
	};
});






