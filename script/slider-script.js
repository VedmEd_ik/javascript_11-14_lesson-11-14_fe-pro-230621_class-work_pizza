$(window).ready(function () {
	const images = $('.slider .slider-line img');
	const sliderLine = $('.slider-line');
	const slider = $('.slider')[0];

	let count = 0;
	let width;
	const timeAutoRollSlider = 15000;	// Швидкість зміни слайду

	// Робимо слайдер адаптивним до зміни розміру екрану
	function init() {
		width = slider.offsetWidth;
		sliderLine.css({
			'width': `${width * images.length}px`,
		});
		images.css({
			'width': `${width}px`,
			'height': 'auto',
		})

		rollSlider();
	}

	window.addEventListener('resize', init);
	init();

	// Робимо прокрутку слайдера
	function rollSlider() {
		sliderLine.css({
			'transform': `translateX(-${count * width}px)`,
		})
	}

	// Прокрутка назад
	$('.btn-prev').click(function () {
		autoRollSliderStop(true);	// Зупиняємо автоматичну прокрутку

		// Прокручуємо слайд
		count--;
		if (count < 0) {
			count = images.length - 1;
		}
		rollSlider();

		autoRollSliderStop(false);	// Запускаємо автоматичну прокрутку
	});

	// Прокрутка вперед

	// Призначаємо обробник для кнопки вперед
	$('.btn-next').click(function () {
		autoRollSliderStop(true);	// Зупиняємо автоматичну прокрутку
		rollNextSlide();	// Прокручуємо слайд
		autoRollSliderStop(false);	// Запускаємо автоматичну прокрутку
	});

	// Автоматична прокрутка	

	// Функція прокрутки слайдера
	function rollNextSlide() {
		count++;
		if (count >= images.length) {
			count = 0;
		}
		rollSlider();
	}

	let autoRollSlider = setInterval(rollNextSlide, timeAutoRollSlider);		// Запускаємо автоматичну прокрутку

	// Функція зупинки/старту автоматичної прокрутки слайдера
	function autoRollSliderStop(stopAutoRollSlider) {
		if (stopAutoRollSlider === true) {
			clearInterval(autoRollSlider);
		} else if (stopAutoRollSlider === false) {
			autoRollSlider = setInterval(rollNextSlide, timeAutoRollSlider);
		}

	}
});


